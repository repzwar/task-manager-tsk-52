package ru.pisarev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.service.IMessageService;
import ru.pisarev.tm.dto.LoggerDTO;

import javax.jms.*;

import static ru.pisarev.tm.constant.ActiveMQConst.STRING;
import static ru.pisarev.tm.constant.ActiveMQConst.URL;
import static ru.pisarev.tm.util.DataUtil.getDate;

public class MessageService implements IMessageService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public MessageService() {
        connectionFactory = new ActiveMQConnectionFactory(URL);
    }

    @Override
    @SneakyThrows
    public void sendMessage(@NotNull final LoggerDTO entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(STRING);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public LoggerDTO prepareMessage(
            @Nullable final Object record,
            @NotNull final String type
    ) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(record);
        @Nullable final String className = record == null ? null : record.getClass().getSimpleName();
        @NotNull final LoggerDTO message = new LoggerDTO(
                json,
                getDate(),
                className,
                type
        );
        return message;
    }

}
