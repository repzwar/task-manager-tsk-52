package ru.pisarev.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.dto.ProjectRecord;
import ru.pisarev.tm.dto.SessionRecord;
import ru.pisarev.tm.dto.TaskRecord;
import ru.pisarev.tm.dto.UserRecord;
import ru.pisarev.tm.exception.system.DatabaseInitException;
import ru.pisarev.tm.model.ProjectGraph;
import ru.pisarev.tm.model.SessionGraph;
import ru.pisarev.tm.model.TaskGraph;
import ru.pisarev.tm.model.UserGraph;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static ru.pisarev.tm.constant.PropertyConst.CACHE_HAZELCAST_USE_LITE_MEMBER;

public final class ConnectionService implements ru.pisarev.tm.api.service.IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = factory();
    }

    @Override
    public @NotNull EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new DatabaseInitException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new DatabaseInitException();
        @Nullable final String password = propertyService.getJdbcPassword();
        if (password == null) throw new DatabaseInitException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new DatabaseInitException();
        @Nullable final String dialect = propertyService.getHibernateDialect();
        if (dialect == null) throw new DatabaseInitException();
        @Nullable final String auto = propertyService.getHibernateBM2DDLAuto();
        if (auto == null) throw new DatabaseInitException();
        @Nullable final String sqlShow = propertyService.getHibernateShowSql();
        if (sqlShow == null) throw new DatabaseInitException();

        @Nullable final String secondLevelCash = propertyService.getSecondLevelCash();
        if (secondLevelCash == null) throw new DatabaseInitException();
        @Nullable final String queryCache = propertyService.getQueryCache();
        if (queryCache == null) throw new DatabaseInitException();
        @Nullable final String minimalPuts = propertyService.getMinimalPuts();
        if (minimalPuts == null) throw new DatabaseInitException();
        @Nullable final String regionPrefix = propertyService.getRegionPrefix();
        if (regionPrefix == null) throw new DatabaseInitException();
        @Nullable final String cacheProvider = propertyService.getCacheProvider();
        if (cacheProvider == null) throw new DatabaseInitException();
        @Nullable final String factoryClass = propertyService.getFactoryClass();
        if (factoryClass == null) throw new DatabaseInitException();
        @Nullable final String liteMember = propertyService.getLiteMember();
        if (liteMember == null) throw new DatabaseInitException();


        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlShow);

        if ("true".equals(secondLevelCash)) {
            settings.put(org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE, secondLevelCash);
            settings.put(org.hibernate.cfg.Environment.USE_QUERY_CACHE, queryCache);
            settings.put(org.hibernate.cfg.Environment.USE_MINIMAL_PUTS, minimalPuts);
            settings.put(org.hibernate.cfg.Environment.CACHE_REGION_PREFIX, regionPrefix);
            settings.put(org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG, cacheProvider);
            settings.put(org.hibernate.cfg.Environment.CACHE_REGION_FACTORY, factoryClass);
            settings.put(CACHE_HAZELCAST_USE_LITE_MEMBER, liteMember);
        }

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectRecord.class);
        sources.addAnnotatedClass(TaskRecord.class);
        sources.addAnnotatedClass(SessionRecord.class);
        sources.addAnnotatedClass(UserRecord.class);

        sources.addAnnotatedClass(ProjectGraph.class);
        sources.addAnnotatedClass(TaskGraph.class);
        sources.addAnnotatedClass(SessionGraph.class);
        sources.addAnnotatedClass(UserGraph.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
