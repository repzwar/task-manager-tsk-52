package ru.pisarev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.dto.ITaskRecordRepository;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.api.service.dto.ITaskRecordService;
import ru.pisarev.tm.dto.TaskRecord;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.repository.dto.TaskRecordRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskRecordService extends AbstractRecordService<TaskRecord> implements ITaskRecordService {

    @NotNull
    public TaskRecordService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public ITaskRecordRepository getRepository() {
        return new TaskRecordRepository(connectionService.getEntityManager());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskRecord> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<TaskRecord> collection) {
        if (collection == null) return;
        for (TaskRecord item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord add(@Nullable final TaskRecord entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    private void update(@NotNull final TaskRecord entity, @NotNull EntityManager entityManager) {
        @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
        repository.update(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final TaskRecord entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            repository.removeById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            return taskRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            taskRepository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            taskRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository taskRepository = new TaskRecordRepository(entityManager);
            @NotNull final TaskRecord task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            update(task, entityManager);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Nullable
    public TaskRecord add(String user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskRecord task = new TaskRecord(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskRecord> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<TaskRecord> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            new TaskRecordRepository(entityManager);
            addAll(collection);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord add(final String user, @Nullable final TaskRecord entity) {
        if (entity == null) return null;
        entity.setUserId(user);
        @Nullable final TaskRecord entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            repository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            repository.removeByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final TaskRecord entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRecordRepository repository = new TaskRecordRepository(entityManager);
            repository.removeByIdUserId(userId, entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
