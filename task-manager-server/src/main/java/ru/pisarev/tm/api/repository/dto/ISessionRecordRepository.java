package ru.pisarev.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.dto.SessionRecord;

import java.util.List;

public interface ISessionRecordRepository {


    List<SessionRecord> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void add(final SessionRecord session);

    void update(final SessionRecord session);

    List<SessionRecord> findAll();

    SessionRecord findById(final String id);

    void clear();

    void removeById(final String id);

}
