package ru.pisarev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.repository.dto.IProjectRecordRepository;
import ru.pisarev.tm.dto.ProjectRecord;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRecordRepository extends AbstractRecordRepository<ProjectRecord> implements IProjectRecordRepository {

    public ProjectRecordRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public ProjectRecord findById(@Nullable final String id) {
        return entityManager.find(ProjectRecord.class, id);
    }

    public void remove(final ProjectRecord entity) {
        ProjectRecord reference = entityManager.getReference(ProjectRecord.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeById(@Nullable final String id) {
        ProjectRecord reference = entityManager.getReference(ProjectRecord.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<ProjectRecord> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectRecord e", ProjectRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public List<ProjectRecord> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectRecord e WHERE e.userId = :userId", ProjectRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public ProjectRecord findByIdUserId(String userId, String id) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectRecord e WHERE e.id = :id AND e.userId = :userId", ProjectRecord.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }


    @Nullable
    @Override
    public ProjectRecord findByName(@NotNull final String userId, @Nullable final String name) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectRecord e WHERE e.name = :name AND e.userId = :userId", ProjectRecord.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public ProjectRecord findByIndex(@NotNull final String userId, final int index) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM ProjectRecord e WHERE e.userId = :userId", ProjectRecord.class)
                        .setParameter("userId", userId)
                        .setFirstResult(index)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectRecord e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectRecord e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectRecord e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectRecord e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM ProjectRecord e WHERE e.userId = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }


}