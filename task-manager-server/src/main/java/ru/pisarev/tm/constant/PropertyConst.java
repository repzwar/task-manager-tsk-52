package ru.pisarev.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface PropertyConst {

    @NotNull
    String FILE_NAME = "application.properties";

    @NotNull
    String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    String PASSWORD_SECRET_VALUE = "";

    @NotNull
    String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    String PASSWORD_ITERATION_VALUE = "1";

    @NotNull
    String APPLICATION_VERSION_KEY = "version";

    @NotNull
    String APPLICATION_VERSION_VALUE = "0.25.0";

    @NotNull
    String SERVER_HOST_KEY = "host";

    @NotNull
    String SERVER_HOST_VALUE = "localhost";

    @NotNull
    String SERVER_PORT_KEY = "port";

    @NotNull
    String SERVER_PORT_VALUE = "8080";

    @NotNull
    String SIGNATURE_SECRET_KEY = "sign.secret";

    @NotNull
    String SIGNATURE_SECRET_VALUE = "";

    @NotNull
    String SIGNATURE_ITERATION_KEY = "sign.iteration";

    @NotNull
    String SIGNATURE_ITERATION_VALUE = "1";

    @NotNull
    String JDBC_USER_KEY = "jdbc.user";

    @NotNull
    String JDBC_PASSWORD_KEY = "jdbc.password";

    @NotNull
    String JDBC_URL_KEY = "jdbc.url";

    @NotNull
    String JDBC_DRIVER_KEY = "jdbc.driver";

    @NotNull
    String HIBERNATE_DIALECT_KEY = "hibernate.dialect";

    @NotNull
    String HIBERNATE_HBM2DDL_AUTO_KEY = "hibernate.hbm2ddl_auto";

    @NotNull
    String HIBERNATE_SHOW_SQL_KEY = "hibernate.show_sql";

    @NotNull
    String CACHE_USE_SECOND_LEVEL_CACHE = "hibernate.cache.use_second_level_cache";

    @NotNull
    String CACHE_USE_QUERY_CACHE = "hibernate.cache.use_query_cache";

    @NotNull
    String CACHE_USE_MINIMAL_PUTS = "hibernate.cache.use_minimal_puts";

    @NotNull
    String CACHE_HAZELCAST_USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    String CACHE_REGION_PREFIX = "hibernate.cache.region_prefix";

    @NotNull
    String CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH = "hibernate.cache.provider_configuration_file_resource_path";

    @NotNull
    String CACHE_REGION_FACTORY_CLASS = "hibernate.cache.region.factory_class";

}
