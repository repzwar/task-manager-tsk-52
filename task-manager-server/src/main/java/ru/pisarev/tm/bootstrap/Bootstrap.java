package ru.pisarev.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.api.service.ILogService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.api.service.dto.*;
import ru.pisarev.tm.api.service.model.IProjectService;
import ru.pisarev.tm.api.service.model.ISessionService;
import ru.pisarev.tm.api.service.model.ITaskService;
import ru.pisarev.tm.api.service.model.IUserService;
import ru.pisarev.tm.component.Backup;
import ru.pisarev.tm.component.MessageExecutor;
import ru.pisarev.tm.dto.ProjectRecord;
import ru.pisarev.tm.dto.TaskRecord;
import ru.pisarev.tm.dto.UserRecord;
import ru.pisarev.tm.endpoint.*;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.service.ConnectionService;
import ru.pisarev.tm.service.DataService;
import ru.pisarev.tm.service.LogService;
import ru.pisarev.tm.service.PropertyService;
import ru.pisarev.tm.service.dto.*;
import ru.pisarev.tm.service.model.ProjectGraphService;
import ru.pisarev.tm.service.model.SessionGraphService;
import ru.pisarev.tm.service.model.TaskGraphService;
import ru.pisarev.tm.service.model.UserGraphService;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.pisarev.tm.util.SystemUtil.getPID;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private static final MessageExecutor messageExecutor = new MessageExecutor();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskRecordService taskRecordService = new TaskRecordService(connectionService);

    @NotNull
    private final IProjectRecordService projectRecordService = new ProjectRecordService(connectionService);

    @NotNull
    private final IProjectTaskRecordService projectTaskRecordService = new ProjectTaskRecordService(connectionService);

    @NotNull
    private final IUserRecordService userRecordService = new UserRecordService(connectionService, propertyService);

    @NotNull
    private final ISessionRecordService sessionRecordService = new SessionRecordService(connectionService, userRecordService, propertyService);

    @NotNull
    private final ITaskService taskService = new TaskGraphService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectGraphService(connectionService);

    @NotNull
    private final IUserService userService = new UserGraphService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionGraphService(connectionService, userService, propertyService);

    @NotNull
    private final DataService dataService = new DataService(userRecordService, taskRecordService, projectRecordService, sessionRecordService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectRecordService, projectTaskRecordService, projectService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionRecordService, userRecordService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskRecordService, projectTaskRecordService, taskService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this, userRecordService, userService, sessionRecordService);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this, dataService);

    public void start(String... args) {
        displayWelcome();
        logService.debug("Test environment.");
        //backup.init();
        initEndpoint();
        initData();
    }

    public void initApplication() {
        initPID();
    }

    private void initData() {
        final String admin = userRecordService.add("admin", "admin", "admin@a").getId();
        @NotNull final UserRecord user = userRecordService.add("user", "user");

        projectRecordService.add(admin, new ProjectRecord("Project C", "-")).setStatus(Status.COMPLETED);
        projectRecordService.add(admin, new ProjectRecord("Project A", "-"));
        projectRecordService.add(admin, new ProjectRecord("Project B", "-")).setStatus(Status.IN_PROGRESS);
        projectRecordService.add(admin, new ProjectRecord("Project D", "-")).setStatus(Status.COMPLETED);
        taskRecordService.add(admin, new TaskRecord("Task C", "-")).setStatus(Status.COMPLETED);
        taskRecordService.add(admin, new TaskRecord("Task A", "-"));
        taskRecordService.add(admin, new TaskRecord("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskRecordService.add(admin, new TaskRecord("Task D", "-")).setStatus(Status.COMPLETED);
    }


    public void initEndpoint() {
        initEndpoint(projectEndpoint);
        initEndpoint(sessionEndpoint);
        initEndpoint(taskEndpoint);
        initEndpoint(adminEndpoint);
        initEndpoint(dataEndpoint);
    }

    @SneakyThrows
    public void initJMSBroker() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    public void initEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public static void sendMessage(@Nullable final Object record,
                                   @NotNull final String type) {
        messageExecutor.sendMessage(record, type);
    }

}
