package component;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IReceiverService;
import ru.pisarev.tm.listener.LogMessageListener;
import ru.pisarev.tm.service.ReceiverService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LoggerExecutor {

    private static final int THREAD_COUNT = 3;


    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void log(@NotNull final String[] list) {
        for (String item : list) {
            es.submit(() -> logSingle("failover://tcp://"+item));
        }
    }

    public void stop() {
        es.shutdown();
    }

    public void logSingle(@NotNull final String url) {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(url);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LogMessageListener());
    }

}
