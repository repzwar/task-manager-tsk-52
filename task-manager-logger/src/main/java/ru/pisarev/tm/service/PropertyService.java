package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

import static ru.pisarev.tm.constant.PropertyConst.FILE_NAME;
import static ru.pisarev.tm.constant.PropertyConst.URL;

public final class PropertyService implements ru.pisarev.tm.api.service.IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @Override
    @Nullable
    public String getUrl() {
        if (System.getenv().containsKey(URL)) {
            @NotNull final String value = System.getenv(URL);
            return value;
        }
        if (System.getProperties().containsKey(URL)) {
            @NotNull final String value = System.getProperty(URL);
            return value;
        }
        @Nullable final String value = properties.getProperty(URL);
        return value;
    }


}
