package ru.pisarev.tm.bootstrap;

import component.LoggerExecutor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.service.IPropertyService;
import ru.pisarev.tm.service.PropertyService;

import static ru.pisarev.tm.constant.ActiveMQConst.URL;

public final class Bootstrap {

    public void start() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final LoggerExecutor executor = new LoggerExecutor();
        @Nullable final String loggerURL = propertyService.getUrl();

        if (loggerURL == null || loggerURL.isEmpty())
            executor.logSingle(URL);
        else {
            @NotNull final String[] list = loggerURL.split(";");
            executor.log(list);
        }
    }

}
