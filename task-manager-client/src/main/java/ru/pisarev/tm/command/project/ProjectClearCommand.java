package ru.pisarev.tm.command.project;

import ru.pisarev.tm.command.ProjectAbstractCommand;

public class ProjectClearCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    public void execute() {
        serviceLocator.getProjectEndpoint().clearProject(getSession());
    }
}
