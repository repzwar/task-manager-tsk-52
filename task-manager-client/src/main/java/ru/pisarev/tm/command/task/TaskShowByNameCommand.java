package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.util.TerminalUtil;

public class TaskShowByNameCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final TaskDto task = serviceLocator.getTaskEndpoint().findTaskByName(getSession(), name);
        if (task == null) throw new TaskNotFoundException();
        show(task);
    }
}
